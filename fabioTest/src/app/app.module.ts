import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {  platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {CompilerConfig} from '@angular/compiler'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { FormsModule, NgModel } from '@angular/forms';
import { FilterPipe } from './filter.pipe';
import {MatDialog} from '@angular/material/dialog';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeroesComponent,
    HeroDetailComponent, 
    FilterPipe,
    NgModel,
    
  
  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
